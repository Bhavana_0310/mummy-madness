import math
import sys
def euclidean_distance(x_tar, y_tar, x_mum, y_mum):
    return math.sqrt((x_tar - x_mum) ** 2 + (y_tar - y_mum) ** 2)

def mummy_madness(mummies):
    time_steps = []# Calculate the time step for each mummy to reach the origin
    for mummy in mummies:
        time_step = int(euclidean_distance(0 , 0 , mummy[0] , mummy[1]))
        if time_step <= 1:
            return -1 
        time_steps.append(time_step)
    time_steps.sort()
    for i in range(len(time_steps)-1):
        if time_steps[i] <= time_steps[i + 1]:
            return (time_steps[i] - 1)# caught after i + 1 time steps
       
def readlines():
    inp = sys.stdin.readlines()# Input reading and processing
    num_of_mummies = int(inp[0])
    mummies = [tuple(map(int,inp[i].split())) for i in range(1 , num_of_mummies + 1)]
    result = mummy_madness(mummies)
    print(result)
readlines()

